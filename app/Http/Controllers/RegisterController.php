<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register(){
        return view('Register');
    }

    public function welcome(Request $request){
        return view('welcome');
    }

    public function welcome_post(Request $request){
        
        $nama=$request->firstname;
        $akhir=$request->lastname;
        return view('welcome',compact('nama','akhir'));
    }
}
