<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf 
        <label>First name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastname"> <br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender" value="0">Male<br>
        <input type="radio" name="gender" value="1">Female<br>
        <input type="radio" name="gender" value="2">Other<br>
        <br>
        <label>Nationality:</label> <br><br>
        <select>
            <option value="id">Indonesia</option>
            <option value="ing">Inggris</option>
            <option value="jpn">Jepang</option>
        </select><br><br>
        <label>Laguage Spoken:</label><br><br>
        <input type="checkbox" name="laguage" value="0">Bahasa Indonesia<br>
        <input type="checkbox" name="laguage" value="1">English<br>
        <input type="checkbox" name="laguage" value="2">Other<br>
        <p>Bio:</p>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up" >

    </form>
</body>
</html>