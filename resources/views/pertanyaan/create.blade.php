@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header">
        <h3 class="card-title">Pertanyaan</h3>
    </div>
    <form role="form" action="/pertanyaan" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="tittle">Judul</label>
                <input type="tittle" class="form-control" name="tittle" value="{{ old('tittle','') }}" id="tittle" placeholder="Judul pertanyaan" required>
            
                @error('tittle')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror  
                
            </div>
            <div class="form-group">
                <label for="body">Ajukan Pertanyaan</label>
                <textarea class="form-control" rows="5" id="body" name="pertanyaan" value="{{ old('pertanyaan','') }}" placeholder="Tulis pertanyaan anda disini" require></textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
           
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection