@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header">
        <h3 class="card-title">Edit Pertanyaan {{$pertanyaan->id}}</h3>
    </div>
    <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="tittle">Judul</label>
                <input type="tittle" class="form-control" name="tittle" value="{{ old('tittle',$pertanyaan->judul) }}" id="tittle" placeholder="Judul pertanyaan" required>
            
                @error('tittle')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror  
                
            </div>
            <div class="form-group">
                <label for="body">Ajukan Pertanyaan</label>
                <textarea class="form-control" rows="5" id="body" name="pertanyaan" value="{{ old('pertanyaan',$pertanyaan->isi) }}" placeholder="Tulis pertanyaan anda disini" require>{{$pertanyaan->isi}}</textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
           
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection