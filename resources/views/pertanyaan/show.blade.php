@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3">
        <div class="card">
              <div class="card-body">
                <h5 class="card-title">{{ $pertanyaan->judul }}</h5>

                <p class="card-text">
                {{ $pertanyaan->isi }}
                </p>
        </div>
  
@endsection